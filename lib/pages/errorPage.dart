import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key? key, required this.onRetry}) : super(key: key);
  final Function onRetry;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: TextButton(
          style: ButtonStyle(
              padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                  const EdgeInsets.symmetric(vertical: 12, horizontal: 42)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14),
                      side: const BorderSide(
                          color: Color.fromARGB(255, 125, 125, 125))))),
          onPressed: () => onRetry.call(),
          child: const Text(
            'Reload',
            style: TextStyle(color: Color.fromARGB(255, 87, 87, 87)),
          )),
    );
  }
}
