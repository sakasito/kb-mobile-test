import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kb_mobile_test/bloc/accountBloc/accountBloc.dart';
import 'package:kb_mobile_test/bloc/billedBloc/billedBloc.dart';
import 'package:kb_mobile_test/bloc/unbillBloc/unbilledBloc.dart';
import 'package:kb_mobile_test/pages/errorPage.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:kb_mobile_test/pages/homePage/accountInfoTab.dart';
import 'package:kb_mobile_test/pages/homePage/billInfo.dart';
import 'package:kb_mobile_test/pages/homePage/unBilledInfo.dart';
import 'package:kb_mobile_test/util/dateHandler.dart';
import 'package:kb_mobile_test/widgets/dashedLine.dart';
import 'package:kb_mobile_test/widgets/monthDropdown.dart';
import 'package:kb_mobile_test/widgets/noResult.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final CarouselController carouselController = CarouselController();

  final List<String> options = ['ACCOUNT INFO', 'UNBILLED', 'BILLED'];

  late String focusedTab = options[0];
  late int cardIndex = 0;

  late String? selectedMonth;
  late List<String> monthList = [];

  @override
  void initState() {
    super.initState();
    context.read<AccountBloc>().add(const LoadAccountDeatil());
    monthList = getMonthList();
    selectedMonth = monthList[6];
  }

  void onOptionSelect(String name, String cardNumber) {
    setState(() {
      focusedTab = name;
    });
    if (name == 'UNBILLED') {
      context
          .read<UnbilledBloc>()
          .add(LoadUnbilledDetail(cardNumber: cardNumber));
    }
    if (name == 'BILLED') {
      context.read<BilledBloc>().add(LoadBilledDetail(
          cardNumber: cardNumber, date: convertMonthToNumber(selectedMonth!)));
    }
  }

  Widget selectableoption(String name, String cardNumber) {
    return Expanded(
      child: InkWell(
        onTap: () => onOptionSelect(name, cardNumber),
        child: Container(
          decoration: BoxDecoration(
              border: name == focusedTab
                  ? const Border(
                      bottom:
                          BorderSide(color: Color.fromARGB(255, 118, 150, 176)))
                  : const Border(bottom: BorderSide.none)),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 17),
            child: Center(
                child: Text(
              name,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: name == focusedTab
                      ? const Color.fromARGB(255, 118, 150, 176)
                      : Colors.grey),
            )),
          ),
        ),
      ),
    );
  }

  List<String> getMonthList() {
    List<String> months = [];
    DateTime currentDate = DateTime.now();
    for (int i = -6; i <= 6; i++) {
      DateTime monthDate = DateTime(currentDate.year, currentDate.month + i);
      months.add(dateToShortMonth(monthDate));
    }
    return months;
  }

  void onMonthSelected(String selected, String cardNumber) {
    setState(() {
      selectedMonth = selected;
    });
    context.read<BilledBloc>().add(LoadBilledDetail(
        cardNumber: cardNumber, date: convertMonthToNumber(selected)));
  }

  Widget accountTab(accountState) {
    return AccountInfoTab(
      cardIndex: cardIndex,
      state: accountState,
    );
  }

  Widget unbilledTab(String cardNumber) {
    return BlocBuilder<UnbilledBloc, UnbilledState>(
      builder: (context, unBilledState) {
        switch (unBilledState.runtimeType) {
          case UnbilledLoading:
            return const Padding(
              padding: EdgeInsets.only(top: 50),
              child: CircularProgressIndicator(),
            );
          case UnbilledComplete:
            if (unBilledState.unbilledDetail.isEmpty) {
              return NoResult(
                text: 'NO TRANSACTION FOUND',
              );
            } else {
              return UnBilledInfo(
                state: unBilledState,
              );
            }
          case UnbilledError:
          default:
            return Padding(
              padding: const EdgeInsets.only(top: 50),
              child: ErrorPage(
                onRetry: () => {
                  context
                      .read<UnbilledBloc>()
                      .add(LoadUnbilledDetail(cardNumber: cardNumber))
                },
              ),
            );
        }
      },
    );
  }

  Widget billedTab(String cardNumber) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'STATEMENT OF',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
              ),
              MonthDropdown(
                cardNumber: cardNumber,
                cardIndex: cardIndex,
                monthList: monthList,
                onMonthSelected: onMonthSelected,
                selectedMonth: '$selectedMonth',
              ),
            ],
          ),
        ),
        const DashedLine(),
        BlocBuilder<BilledBloc, BilledState>(
          builder: (context, billedState) {
            switch (billedState.runtimeType) {
              case BilledLoading:
                return const Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: CircularProgressIndicator(),
                );
              case BilledComplete:
                if (billedState.billedDetail.isEmpty) {
                  return NoResult(
                    text: 'NO TRANSACTION FOUND',
                  );
                } else {
                  return BillInfo(
                    billedState: billedState,
                  );
                }
              default:
                return Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: ErrorPage(
                    onRetry: () => {
                      context.read<BilledBloc>().add(LoadBilledDetail(
                          cardNumber: cardNumber,
                          date: convertMonthToNumber(selectedMonth!)))
                    },
                  ),
                );
            }
          },
        ),
      ],
    );
  }

  Widget renderTabFromOption(AccountState accountState) {
    switch (focusedTab) {
      case 'ACCOUNT INFO':
        return accountTab(accountState);
      case 'UNBILLED':
        return unbilledTab(
            accountState.cardDetail.creditCard[cardIndex].cardNumber);
      case 'BILLED':
        return billedTab(
            accountState.cardDetail.creditCard[cardIndex].cardNumber);
      default:
        return Container();
    }
  }

  Widget tabsContents(AccountState state) {
    return Column(
      children: [
        Container(
          decoration: const BoxDecoration(
              border: Border.symmetric(
                  horizontal: BorderSide(
            color: Color.fromARGB(255, 181, 181, 181),
          ))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ...(options.map((e) => selectableoption(
                  e, state.cardDetail.creditCard[cardIndex].cardNumber)))
            ],
          ),
        ),
        renderTabFromOption(state)
      ],
    );
  }

  void onCardSlide(int index) {
    context.read<UnbilledBloc>().add(const ResetUnbilledList());
    context.read<BilledBloc>().add(const ResetBilledDetail());

    setState(() {
      selectedMonth = monthList[6];
      cardIndex = index;
      focusedTab = 'ACCOUNT INFO';
    });
  }

  Widget renderCompleteState(AccountState state) {
    return SingleChildScrollView(
      physics: const ScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.only(top: 50),
        child: Column(
          children: [
            CarouselSlider.builder(
              itemCount: state.cardDetail.creditCard.length,
              options: CarouselOptions(
                  viewportFraction: 0.85,
                  initialPage: 0,
                  enableInfiniteScroll: false,
                  onPageChanged: ((index, reason) => onCardSlide(index))),
              itemBuilder: (BuildContext context, int index, int realIndex) {
                return Image.network(
                    state.cardDetail.creditCard[index].cardImageUrl);
              },
            ),
            tabsContents(state),
          ],
        ),
      ),
    );
  }

  Widget renderHomePageFromState(AccountState state) {
    switch (state.runtimeType) {
      case AccountLoading:
        return const Center(
          child: CircularProgressIndicator(),
        );
      case AccountComplete:
        return renderCompleteState(state);
      default:
        return ErrorPage(
          onRetry: () =>
              {context.read<AccountBloc>().add(const LoadAccountDeatil())},
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: BlocBuilder<AccountBloc, AccountState>(
      builder: (context, state) {
        return Scaffold(
          body: renderHomePageFromState(state),
        );
      },
    ));
  }
}
