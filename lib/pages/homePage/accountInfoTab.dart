import 'package:flutter/material.dart';
import 'package:kb_mobile_test/bloc/accountBloc/accountBloc.dart';
import 'package:kb_mobile_test/util/currencyHandler.dart';
import 'package:kb_mobile_test/util/dateHandler.dart';
import 'package:kb_mobile_test/widgets/rowContent.dart';

class AccountInfoTab extends StatelessWidget {
  AccountState state;
  int cardIndex;
  AccountInfoTab({super.key, required this.state, required this.cardIndex});
  @override
  Widget build(BuildContext context) {
    TextStyle w600 = const TextStyle(fontWeight: FontWeight.w600);
    TextStyle w600s30 =
        const TextStyle(fontWeight: FontWeight.w600, fontSize: 30);
    TextStyle w600s18 =
        const TextStyle(fontWeight: FontWeight.w600, fontSize: 18);

    return Padding(
      padding: const EdgeInsets.fromLTRB(17, 17, 17, 30),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 60),
            child: RowContent(
              topLeftTitle: Text(
                'AVAILABLE CREDIT',
                style: w600,
              ),
              leftContent: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  convertDoubleCurrency(
                      state.cardDetail.creditCard[cardIndex].availableCredit),
                  style: w600s30,
                ),
              ),
              topRightTitle: Text(
                'CREDIT LIMIT',
                style: w600,
              ),
              rightContent: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  convertDoubleCurrency(
                      state.cardDetail.creditCard[cardIndex].creditLimit),
                  style: w600s30,
                ),
              ),
            ),
          ),
          RowContent(
            topLeftTitle: Text(
              'MIN PAY',
              style: w600,
            ),
            leftContent: Text(
              convertDoubleCurrency(
                  state.cardDetail.creditCard[cardIndex].minPay),
              style: w600s18,
            ),
            topRightTitle: Text(
              'FULL PAY',
              style: w600,
            ),
            rightContent: Text(
              convertDoubleCurrency(
                  state.cardDetail.creditCard[cardIndex].fullPay),
              style: w600s18,
            ),
          ),
          const Divider(
            thickness: 1,
          ),
          RowContent(
            topLeftTitle: Text(
              'STATEMENT DATE',
              style: w600,
            ),
            leftContent: Text(
              formatDateTimet(
                  state.cardDetail.creditCard[cardIndex].statementDate),
              style: w600s18,
            ),
            topRightTitle: Text('DUE DATE', style: w600),
            rightContent: Text(
              formatDateTimet(state.cardDetail.creditCard[cardIndex].dueDate),
              style: w600s18,
            ),
          ),
        ],
      ),
    );
  }
}
