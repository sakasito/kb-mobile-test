import 'package:flutter/material.dart';
import 'package:kb_mobile_test/bloc/billedBloc/billedBloc.dart';
import 'package:kb_mobile_test/widgets/bill.dart';

class BillInfo extends StatelessWidget {
  BilledState billedState;
  BillInfo({super.key, required this.billedState});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          Column(children: [
            ...(billedState.billedDetail.map(
              (bill) => Bill(
                amount: bill.amount,
                description: bill.description,
                statementDate: bill.statementDate,
              ),
            )),
          ])
        ],
      ),
    );
  }
}
