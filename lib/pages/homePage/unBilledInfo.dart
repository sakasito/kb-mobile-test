import 'package:flutter/material.dart';
import 'package:kb_mobile_test/bloc/unbillBloc/unbilledBloc.dart';
import 'package:kb_mobile_test/widgets/bill.dart';

class UnBilledInfo extends StatelessWidget {
  UnbilledState state;

  UnBilledInfo({required this.state});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(children: [
        ...(state.unbilledDetail.map((bill) => Bill(
              amount: bill.amount,
              description: bill.description,
              statementDate: bill.statementDate,
            ))),
      ]),
    );
  }
}
