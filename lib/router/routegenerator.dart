import 'package:flutter/material.dart';
import 'package:kb_mobile_test/pages/homePage/homePage.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const MyHomePage());
      default:
        return MaterialPageRoute(
          builder: (_) => const MyHomePage(),
        );
    }
  }
}
