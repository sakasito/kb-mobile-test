import 'dart:convert' as convert;

import 'package:kb_mobile_test/models/cardDetail.dart';
import 'package:kb_mobile_test/services/service.dart';

List<CreditCard> parseCardDetail(String responseBody) {
  try {
    final parsed = convert.jsonDecode(responseBody);
    return List<CreditCard>.from(parsed['cards'].map((x) {
      return CreditCard.fromJson(x);
    }));
  } catch (e) {
    print('catch when parse list e ${e}');
    return [];
  }
}

class CardService {
  Future<CardDetail> getCardList() async {
    try {
      final response = await Service.rest(
        method: 'get',
        url:
            'https://card-management-eajwtocuqa-as.a.run.app/v1/cards/1111111111111',
      );
      if (response.statusCode == 200) {
        final List<CreditCard> creditCard = parseCardDetail(response.body);
        final Map<String, dynamic> parsed = convert.jsonDecode(response.body);
        CardDetail cardDetail =
            CardDetail(citizenId: parsed['citizenId'], creditCard: creditCard);
        return cardDetail;
      } else {
        return throw Exception('Status code: ${response.statusCode}');
      }
    } catch (err) {
      return throw Exception(err);
    }
  }
}
