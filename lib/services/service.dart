import 'package:http/http.dart' as http;

class Service {
  final String method;
  Service({required this.method});

  static Future<http.Response> rest(
      {required String method,
      required String url,
      Map<String, String>? query}) async {
    try {
      http.Response response;
      print('calling ${method} with ${url} query: ${query}');
      switch (method) {
        case 'get':
          response =
              await http.get(Uri.parse(url).replace(queryParameters: query));
          break;
        default:
          response = await http.get(Uri.parse(url));
          break;
      }
      return response;
    } catch (err) {
      http.Response response = http.Response as http.Response;
      return throw Exception(err);
      ;
    }
  }
}
