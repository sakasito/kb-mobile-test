import 'dart:convert' as convert;
import 'package:kb_mobile_test/models/unbillDetail.dart';
import 'package:kb_mobile_test/services/service.dart';

List<BillDetail> parseBilledDetail(String responseBody) {
  try {
    final parsed = convert.jsonDecode(responseBody);
    return List<BillDetail>.from(parsed.map((x) {
      return BillDetail.fromJson(x);
    }));
  } catch (e) {
    print('catch when parse list e ${e}');
    return [];
  }
}

class BillingService {
  Future<List<BillDetail>> getUnbilled(String cardNumber) async {
    try {
      final response = await Service.rest(
        method: 'get',
        url:
            'https://card-management-eajwtocuqa-as.a.run.app/v1/unbilled-statements',
        query: {'cardNumber': cardNumber},
      );
      if (response.statusCode == 200) {
        List<BillDetail> unbilledDetail = parseBilledDetail(response.body);
        return unbilledDetail;
      } else {
        return throw Exception('Status code: ${response.statusCode}');
      }
    } catch (err) {
      return throw Exception(err);
    }
  }

  Future<List<BillDetail>> getBilled(String cardNumber, String date) async {
    try {
      final response = await Service.rest(
        method: 'get',
        url:
            'https://card-management-eajwtocuqa-as.a.run.app/v1/billed-statements',
        query: {'cardNumber': cardNumber, 'asOf': date},
      );
      if (response.statusCode == 200) {
        List<BillDetail> unbilledDetail = parseBilledDetail(response.body);
        return unbilledDetail;
      } else {
        return throw Exception('Status code: ${response.statusCode}');
      }
    } catch (err) {
      return throw Exception(err);
    }
  }
}
