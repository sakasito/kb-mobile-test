part of 'billedBloc.dart';

abstract class BilledState extends Equatable {
  final List<BillDetail> billedDetail;
  BilledState({required this.billedDetail});

  @override
  List<Object> get props => [];
}

class BilledInitial extends BilledState {
  BilledInitial({required super.billedDetail});
}

class BilledLoading extends BilledState {
  BilledLoading({required super.billedDetail});
}

class BilledComplete extends BilledState {
  BilledComplete({required super.billedDetail});
}

class BilledError extends BilledState {
  BilledError({required super.billedDetail});
}
