import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:kb_mobile_test/models/unbillDetail.dart';
import 'package:kb_mobile_test/services/bill/index.dart';

part 'billedEvent.dart';
part 'billedState.dart';

DateTime? billedTimer;
String prevDate = '';

class BilledBloc extends Bloc<BilledEvent, BilledState> {
  BilledBloc() : super(BilledInitial(billedDetail: [])) {
    on<LoadBilledDetail>((event, emit) async {
      try {
        Future getBillingService() async {
          BillingService billingService = BillingService();
          emit(BilledLoading(billedDetail: state.billedDetail));
          final List<BillDetail> billedDetail =
              await billingService.getBilled(event.cardNumber, event.date);
          emit(BilledComplete(billedDetail: billedDetail));
        }

        if (billedTimer == null) {
          prevDate = event.date;
          billedTimer = DateTime.now();
          await getBillingService();
        } else if (DateTime.now().minute > billedTimer!.minute + 5 ||
            prevDate != event.date) {
          prevDate = event.date;
          billedTimer = DateTime.now();
          await getBillingService();
        } else {
          emit(BilledLoading(billedDetail: state.billedDetail));
          emit(BilledComplete(billedDetail: state.billedDetail));
        }
      } catch (err) {
        emit(BilledError(billedDetail: state.billedDetail));
      }
    });
    on<ResetBilledDetail>(
      (event, emit) {
        prevDate = '';
        emit(BilledLoading(billedDetail: []));
        emit(BilledComplete(billedDetail: []));
      },
    );
  }
}
