part of 'billedBloc.dart';

abstract class BilledEvent extends Equatable {
  const BilledEvent();

  @override
  List<Object> get props => [];
}

class LoadBilledDetail extends BilledEvent {
  final String cardNumber;
  final String date;
  const LoadBilledDetail({required this.cardNumber, required this.date});
}

class ResetBilledDetail extends BilledEvent {
  const ResetBilledDetail();
}
