part of 'unbilledBloc.dart';

abstract class UnbilledState extends Equatable {
  late List<BillDetail> unbilledDetail;
  UnbilledState({required this.unbilledDetail});

  @override
  List<Object> get props => [];
}

class UnbilledInitial extends UnbilledState {
  UnbilledInitial({required super.unbilledDetail});
}

class UnbilledLoading extends UnbilledState {
  UnbilledLoading({required super.unbilledDetail});
}

class UnbilledComplete extends UnbilledState {
  UnbilledComplete({required super.unbilledDetail});
}

class UnbilledError extends UnbilledState {
  UnbilledError({required super.unbilledDetail});
}
