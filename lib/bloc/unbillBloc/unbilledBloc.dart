import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kb_mobile_test/models/unbillDetail.dart';
import 'package:kb_mobile_test/services/bill/index.dart';

part 'unbilledEvent.dart';
part 'unbilledState.dart';

DateTime? unbilledTimer;

class UnbilledBloc extends Bloc<UnbilledEvent, UnbilledState> {
  UnbilledBloc() : super(UnbilledInitial(unbilledDetail: [])) {
    on<LoadUnbilledDetail>((event, emit) async {
      try {
        Future callGetUnbilled() async {
          BillingService billingService = BillingService();
          emit(UnbilledLoading(unbilledDetail: state.unbilledDetail));
          final List<BillDetail> unbilledDetail =
              await billingService.getUnbilled(event.cardNumber);

          emit(UnbilledComplete(unbilledDetail: unbilledDetail));
        }

        if (unbilledTimer == null) {
          unbilledTimer = DateTime.now();
          await callGetUnbilled();
        } else if (DateTime.now().minute > unbilledTimer!.minute + 5 ||
            state.unbilledDetail.isEmpty) {
          await callGetUnbilled();
        } else {
          emit(UnbilledLoading(unbilledDetail: state.unbilledDetail));
          emit(UnbilledComplete(unbilledDetail: state.unbilledDetail));
        }
      } catch (err) {
        emit(UnbilledError(unbilledDetail: state.unbilledDetail));
      }
    });
    on<ResetUnbilledList>(
        (event, emit) => {emit(UnbilledInitial(unbilledDetail: const []))});
  }
}
