part of 'unbilledBloc.dart';

abstract class UnbilledEvent extends Equatable {
  const UnbilledEvent();

  @override
  List<Object> get props => [];
}

class LoadUnbilledDetail extends UnbilledEvent {
  final String cardNumber;
  const LoadUnbilledDetail({required this.cardNumber});
}

class ResetUnbilledList extends UnbilledEvent {
  const ResetUnbilledList();
}
