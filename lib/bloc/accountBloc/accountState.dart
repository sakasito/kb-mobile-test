part of 'accountBloc.dart';

abstract class AccountState extends Equatable {
  late CardDetail cardDetail;
  AccountState({required this.cardDetail});

  @override
  List<Object> get props => [];
}

class AccountInitial extends AccountState {
  AccountInitial({required super.cardDetail});
}

class AccountLoading extends AccountState {
  AccountLoading({required super.cardDetail});
}

class AccountComplete extends AccountState {
  AccountComplete({required super.cardDetail});
}

class AccountError extends AccountState {
  AccountError({required super.cardDetail});
}
