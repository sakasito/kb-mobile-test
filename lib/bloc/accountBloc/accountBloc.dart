import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kb_mobile_test/models/cardDetail.dart';
import 'package:kb_mobile_test/services/cards/index.dart';

part 'accountEvent.dart';
part 'accountState.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  AccountBloc() : super(AccountInitial(cardDetail: cardDetailDefault)) {
    on<LoadAccountDeatil>((event, emit) async {
      try {
        CardService cardService = CardService();
        emit(AccountLoading(cardDetail: state.cardDetail));
        CardDetail cardDetail = await cardService.getCardList();
        emit(AccountComplete(cardDetail: cardDetail));
      } catch (err) {
        emit(AccountError(cardDetail: cardDetailDefault));
      }
    });
  }
}
