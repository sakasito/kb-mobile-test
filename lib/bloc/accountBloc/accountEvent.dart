part of 'accountBloc.dart';

abstract class AccountEvent extends Equatable {
  const AccountEvent();

  @override
  List<Object?> get props => [];
}

class LoadAccountDeatil extends AccountEvent {
  const LoadAccountDeatil();
}
