import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kb_mobile_test/bloc/accountBloc/accountBloc.dart';
import 'package:kb_mobile_test/bloc/billedBloc/billedBloc.dart';
import 'package:kb_mobile_test/bloc/unbillBloc/unbilledBloc.dart';
import 'package:kb_mobile_test/router/routegenerator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final accountBloc =
        BlocProvider<AccountBloc>(create: (context) => AccountBloc());
    final unbilledBloc =
        BlocProvider<UnbilledBloc>(create: (context) => UnbilledBloc());
    final billedBloc =
        BlocProvider<BilledBloc>(create: (context) => BilledBloc());
    return MultiBlocProvider(
        providers: [
          accountBloc,
          unbilledBloc,
          billedBloc,
        ],
        child: const MaterialApp(
          initialRoute: '/',
          onGenerateRoute: RouteGenerator.generateRoute,
        ));
  }
}
