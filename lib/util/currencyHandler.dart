import 'package:intl/intl.dart' as intl;

String convertDoubleCurrency(double amount) {
  final oCcy = intl.NumberFormat.simpleCurrency(locale: 'th_TH', name: '');
  final converted = oCcy.format(amount);
  return converted.contains('.00')
      ? converted.replaceAll('.00', '')
      : converted;
}
