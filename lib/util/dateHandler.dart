import 'package:intl/intl.dart';

String convertMonthToNumber(String month) {
  DateTime parsedDate = DateFormat('MMM yyyy').parse(month);
  return DateFormat('MMyyyy').format(parsedDate);
}

String dateToShortMonth(DateTime date) {
  return DateFormat('MMM yyyy').format(date);
}

String formatDateTimet(DateTime date) {
  final DateFormat formatter = DateFormat('dd MMM yyyy');
  return formatter.format(date);
}
