import 'package:flutter/material.dart';

class RowContent extends StatelessWidget {
  final Widget? topLeftTitle;
  final Widget? leftContent;
  final Widget? topRightTitle;
  final Widget? rightContent;

  const RowContent(
      {super.key,
      this.topLeftTitle,
      this.leftContent,
      this.topRightTitle,
      this.rightContent});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            topLeftTitle ?? const Text(''),
            topRightTitle ?? const Text(''),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            leftContent ?? const Text(''),
            rightContent ?? const Text(''),
          ],
        )
      ],
    );
  }
}
