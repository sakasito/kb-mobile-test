import 'package:flutter/material.dart';

class MonthDropdown extends StatelessWidget {
  String selectedMonth;
  late Function onMonthSelected;
  String cardNumber;
  List<String> monthList;
  int cardIndex;
  MonthDropdown(
      {super.key,
      required this.onMonthSelected,
      required this.selectedMonth,
      required this.cardNumber,
      required this.monthList,
      required this.cardIndex});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: Colors.grey)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: DropdownButton<String>(
          alignment: Alignment.center,
          underline: Container(),
          borderRadius: BorderRadius.circular(10),
          value: selectedMonth,
          style:
              const TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
          onChanged: (String? newValue) =>
              onMonthSelected(newValue!, cardNumber),
          items: monthList.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}
