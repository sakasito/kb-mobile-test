import 'package:flutter/material.dart';
import 'package:kb_mobile_test/util/currencyHandler.dart';
import 'package:kb_mobile_test/util/dateHandler.dart';

import 'rowContent.dart';

class Bill extends StatelessWidget {
  String description;
  DateTime statementDate;
  double amount;
  Bill(
      {required this.description,
      required this.statementDate,
      required this.amount});
  @override
  Widget build(BuildContext context) {
    TextStyle w600s18 =
        const TextStyle(fontWeight: FontWeight.w600, fontSize: 18);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 14),
      decoration: const BoxDecoration(
          border: Border(
              bottom: BorderSide(color: Color.fromARGB(255, 181, 181, 181)))),
      child: RowContent(
        topLeftTitle: Text(
          description,
          style: const TextStyle(
              fontWeight: FontWeight.w600, fontSize: 18, fontFamily: 'Arial'),
        ),
        leftContent: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Text(
            dateToShortMonth(statementDate),
            style: w600s18.merge(TextStyle(color: Colors.grey)),
          ),
        ),
        topRightTitle: Text(
          '${convertDoubleCurrency(amount)} THB',
          style: w600s18,
        ),
      ),
    );
  }
}
