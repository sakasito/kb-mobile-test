import 'package:flutter/material.dart';

class NoResult extends StatelessWidget {
  String text;
  NoResult({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50),
      child: Text(
        text,
        style: const TextStyle(
            fontWeight: FontWeight.w600,
            color: Color.fromARGB(255, 172, 172, 172),
            fontSize: 18),
      ),
    );
  }
}
