class BillDetail {
  final String description;
  final DateTime statementDate;
  final double amount;

  const BillDetail({
    required this.description,
    required this.statementDate,
    required this.amount,
  });
  factory BillDetail.fromJson(Map<String, dynamic> json) {
    return BillDetail(
      amount: json['amount'].toDouble(),
      description: json['description'],
      statementDate: DateTime.parse(json['statementDate']),
    );
  }
}

BillDetail billDetailDefault =
    BillDetail(amount: 0, description: '', statementDate: DateTime(0));
