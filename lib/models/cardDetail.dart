class CreditCard {
  final String cardName;
  final String cardHolderName;
  final String cardNumber;
  final String cardImageUrl;
  final double creditLimit;
  final double availableCredit;
  final double minPay;
  final double fullPay;
  final DateTime statementDate;
  final DateTime dueDate;

  CreditCard({
    required this.cardName,
    required this.cardHolderName,
    required this.cardNumber,
    required this.cardImageUrl,
    required this.creditLimit,
    required this.availableCredit,
    required this.minPay,
    required this.fullPay,
    required this.statementDate,
    required this.dueDate,
  });

  factory CreditCard.fromJson(Map<String, dynamic> json) {
    return CreditCard(
      cardName: json['cardName'],
      cardHolderName: json['cardHolderName'],
      cardNumber: json['cardNumber'],
      cardImageUrl: json['cardImageUrl'],
      creditLimit: json['creditLimit'].toDouble(),
      availableCredit: json['availableCredit'].toDouble(),
      minPay: json['minPay'].toDouble(),
      fullPay: json['fullPay'].toDouble(),
      statementDate: DateTime.parse(json['statementDate']),
      dueDate: DateTime.parse(json['dueDate']),
    );
  }
}

class CardDetail {
  final String citizenId;
  List<CreditCard> creditCard;

  CardDetail({required this.citizenId, required this.creditCard});
  factory CardDetail.fromJson(Map<String, dynamic> json) {
    return CardDetail(
        citizenId: json['citizenId'],
        creditCard: CreditCard.fromJson(json['cards']) as List<CreditCard>);
  }
}

CardDetail cardDetailDefault = CardDetail(citizenId: '', creditCard: []);
